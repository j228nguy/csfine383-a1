// Sound Visualizations 

let goFullScreen = true
let mic, fft;
let micSetupError;
let spectrogram;
let backgroundColor;

const numFftBins = 1024;
const showLengthInSeconds = 30;

function setup() {
  let canvasWidth = 800; // windowWidth
  let canvasHeight = 600;
  
  if(goFullScreen){
    canvasWidth = windowWidth;
    canvasHeight = windowHeight;
  }
  createCanvas(canvasWidth, canvasHeight);
  
  // my mic input
  mic = new p5.AudioIn();
  mic.start();

  print(mic);
  
  fft = new p5.FFT(0, numFftBins);
  fft.setInput(mic);
  
  backgroundColor = color(90);
  
  // Split the canvas into different parts for the visualization
  let yTop = 0;
  let yHeight = height / 2;
  spectrogram = new Spectrogram(0, yTop, width, yHeight, backgroundColor, showLengthInSeconds);

  // Create message input box
  field = createInput("title message");
  field.position(10, height-50);
}

function audioInErrorCallback(){
  print("Error setting up the microphone input"); 
}

function keyPressed() {
  // Space to reset all agents
  if (mic.started && key == " ") {
    mic.stop(); 
  }
    else {
      mic.start();
    }    
  // SHIFT-S saves the current canvas
  if (key == 'S') {
    save('messageReceived.png')
  }
}

function draw() {
  background(220);

  let spectrum = fft.analyze();
  
  spectrogram.update(spectrum);
  spectrogram.draw();
  
  //print((waveform.length / sampleRate()) * 1000 + "ms");
  fill(255);
  text("fps: " + nfc(frameRate(), 1), (width / 2) - 175, 15);
  text("GET IT OFF YOUR CHEST", width / 2, 15);
}